;; For initial configuration on top of Prelude's init.el

(require 'my-preload)

;; Config files
(require 'my-init)
(require 'my-helm)
(require 'my-eyecandy)

;; Language-specific options


(require 'my-prose)
(require 'my-org)
(require 'my-markdown)
(require 'my-latex)

(require 'my-python)
(require 'my-elisp)
(require 'my-c)
(require 'my-eyecandy)

(provide 'my-init)
