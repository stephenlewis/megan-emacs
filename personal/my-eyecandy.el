;; Let's make emacs a little prettier.


(use-package solarized-theme
    :ensure solarized-theme
    :config
    (progn
      color-theme-solarized-light ))


;; FONTS: USE SERIF FONTS FOR PROSE

;; use variable-width font for some modes
(defun my-variable-pitch-mode ()
  "Set current buffer to use variable-width font."
  (variable-pitch-mode 1)
  ;; (text-scale-increase 0.5 )
)

(defun my-set-font-to-monospace ()
  "Change font in current window to a monospaced font."
  (interactive)
  (my-variable-pitch-mode)
  (set-frame-font "Inconsolata" t)
  )

(defun my-set-font-to-variable-width ()
  "Change font in current window to a variable-width font."
  (interactive)
  (my-variable-pitch-mode)
  (set-frame-font "Georgia" t)
  )

;; HTML
(add-hook 'html-mode-hook 'my-serif-font)
(add-hook 'xah-html-mode-hook 'my-serif-font)
(add-hook 'nxml-mode-hook 'my-serif-font)

;; Text
(add-hook 'org-mode 'my-serif-font)
(add-hook 'markdown-mode 'my-serif-font)
