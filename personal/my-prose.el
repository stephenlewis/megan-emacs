;; Tools for english prose.

;; Load from personal/dupwords.el
;; This program will find duplicate words in sentences.  For example,
;; in the sentence `The the cat sat on the mat', it will notice that
;; you have written `the the' and highlight the two identical words.
;; To check a region, use `M-x dw-check-region', or use `M-x
;; dw-check-to-end' to check from the current point to the end of the
;; buffer.  When you find a duplicate word, if you press `n' to exit,
;; point is left at the beginning of the first word, and mark at the
;; start of the second.
(add-to-list 'load-path "~/.emacs.d/personal/dupwords/")
(require 'dupwords)

;;;  personal/diction.el
;;;; GNU diction: english prose style critique
;;(add-to-list 'load-path "~/.emacs.d/personal/diction/")
;;(use-package diction)
;;
;;(add-hook 'auto-save-hook 'diction-buffer)
;;(add-hook 'after-save-hook 'diction-buffer)
;;
;;;; Note-taking helper
;;(use-package deft
;;  :ensure deft
;;  :config
;;      (progn (setq deft-extension "org")
;;          (setq deft-text-mode org-mode)
;;          (setq deft-use-filename-as-title t))
;;)
;;

;; Predictive mode

(add-to-list 'load-path "~/.emacs.d/personal/predictive/")
(add-to-list 'load-path "~/.emacs.d/personal/predictive/latex/")
(use-package predictive)

;; Mode hooks
(add-hook 'org-mode-hook 'predictive-mode)
(add-hook 'markdown-mode-hook 'predictive-mode)

;; TODO: fixme
;; (setq auto-completion-syntax-alist (quote (global accept . word))) ;; Wccept the current the most likely completion w/ space, punctuation.
;; (setq auto-completion-min-chars (quote (global . 2))) ;; Avoid completion for short trivial words


(provide 'my-prose)
