(define-package "org-toc" "20141110.650" "add table of contents to org-mode files" 'nil :url "https://github.com/snosov1/org-toc" :keywords '("org-mode" "org" "toc" "table" "of" "contents"))
