;;; org-toc-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (or (file-name-directory #$) (car load-path)))

;;;### (autoloads nil "org-toc" "org-toc.el" (21671 19780 405932
;;;;;;  905000))
;;; Generated autoloads from org-toc.el

(autoload 'org-toc-enable "org-toc" "\
Enable org-toc in this buffer.

\(fn)" nil nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; org-toc-autoloads.el ends here
