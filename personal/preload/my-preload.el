;; For initial configuration on top of Prelude's init.el

;; Use-package

(setq package-enable-at-startup nil)

(package-initialize)


(add-to-list 'load-path "~/.emacs.d/personal/preload/")
(require 'use-package)

(provide 'my-preload)
