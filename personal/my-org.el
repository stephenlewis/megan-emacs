
(require 'my-prose)

(use-package org
  :commands (org-mode org-capture org-agenda orgtbl-mode)
  :init
  (progn
    (add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
    (global-set-key (kbd "C-c c") 'org-capture)
    (global-set-key (kbd "C-c a") 'org-agenda)
    (add-hook 'before-save-hook 'whitespace-cleanup)
    (add-hook 'before-save-hook 'org-table-recalculate-buffer-tables)
    (add-hook 'before-save-hook 'flyspell-buffer)


    (add-hook 'org-mode-hook 'org-toggle-pretty-entities)
    )
  :config
  (progn

    (setq org-odd-levels-only t)
    (setq org-default-notes-file "~/documents/notes/notes.org" org-log-done t)
    (defface org-block-begin-line '((t ( org-meta-line :background "gray27" :overline "gray20" :underline "gray20" :height 0.8)))
      "Face used for the line delimiting the begin of source blocks.")

    (defface org-block-background
      '((t (:background "#FFFFEA")))
      "Face used for the source block background.")

    ;; compile to pdf directly
    (setq org-latex-pdf-process (list "latexmk -f -pdf %f"))

    ;; Use latexmk for PDF export
    (setq org-latex-to-pdf-process (list "latexmk %f"))

    ;; Use appropriate quotation style when exporting to latex
    (setq org-export-latex-quotes
            '(("en" ("\\(\\s-\\|[[(]\\)\"" . "\\enquote{") ("\\(\\S-\\)\"" . "}") ("\\(\\s-\\|(\\)'" . "`"))))

    (setq org-startup-indented t); Use virtual indentation for all files


    (defface org-block-end-line
      '((t ( org-meta-line :background "gray27" :overline "gray20" :underline "gray20" :height 0.8)))
      "Face used for the line delimiting the end of source blocks.")

    (use-package org-autolist
      :ensure org-autolist
      :config
      (progn org-autolist-mode))


    (use-package org-bullets
      :ensure org-bullets
      :init (progn 'org-mode-hook (org-bullets-mode)) )
    (use-package-ensure-elpa 'org-pomodoro)
    (use-package-ensure-elpa 'org-projectile)

    (use-package org-toc
      :ensure org-toc
      :config (progn
                (add-to-list 'load-path "~/.emacs.d/personal/org-toc")
                ;; (add-hook 'org-mode-hook 'org-toc-enable)
                ))


    (use-package-ensure-elpa 'org-transform-tree-table)

    (use-package cdlatex
      :ensure cdlatex
      :init
       (progn ( add-hook 'org-mode-hook 'turn-on-org-cdlatex )))


    (use-package calc)

    (after 'flycheck
      (flyspell-mode))

    (setq org-src-fontify-natively t)
    (use-package ob
      :config
      (progn
	(org-babel-do-load-languages
	 'org-babel-load-languages
	 '((R . t)
	   (emacs-lisp . t)
	   (python . t)
	   ))
	)
      )

    ;;  Org-sync
    (add-to-list 'load-path "~/.emacs.d/personal/org-sync")
    (mapc 'load
         '("os" "os-bb" "os-github" "os-rmine"))
    (require 'os)


  (use-package deft
    :ensure deft
    :config
        (progn (setq deft-extension "org")
            (setq deft-text-mode 'org-mode)
            (setq deft-use-filename-as-title t))
        )

  (use-package org-trello
    :ensure org-trello)

 ))

(provide 'my-org)
